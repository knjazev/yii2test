<?php

use yii\db\Migration;

/**
 * Class m171207_102131_create_table_projects
 */
class m171207_102131_create_table_projects extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {

    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
        // echo "m171207_101011_create_table_users cannot be reverted.\n";

        // return false;
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
				$this->createTable('projects', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'price' => $this->integer()->notNull(),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('projects');
    }
}