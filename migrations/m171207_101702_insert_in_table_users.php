<?php

use yii\db\Migration;

/**
 * Class m171207_101702_insert_in_table_users
 */
class m171207_101702_insert_in_table_users extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {

    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
        // echo "m171207_101702_insert_in_table_users cannot be reverted.\n";

        // return false;
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
				$this->insert('users', [
            'username' => 'admin',
            'login' => 'admin',
            'password' => 'admin',
        ]);
    }

    public function down()
    {
        $this->delete('users', ['login' => 'admin']);
    }
    
}
