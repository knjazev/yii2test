<?php

use yii\db\Migration;

/**
 * Class m171207_101011_create_table_users
 */
class m171207_101011_create_table_users extends Migration
{
    /**
     * @inheritdoc
     */
    // public function safeUp()
    // {

    // }

    /**
     * @inheritdoc
     */
    // public function safeDown()
    // {
        // echo "m171207_101011_create_table_users cannot be reverted.\n";

        // return false;
    // }

    
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
				$this->createTable('users', [
            'id' => $this->primaryKey(),
            'username' => $this->string(32)->notNull(),
            'login' => $this->string(32)->notNull()->unique(),
            'password' => $this->string(20)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('users');
    }
    
}
